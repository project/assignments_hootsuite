<?php

namespace Drupal\assignments_hootsuite\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Class Hootsuite API Client Service.
 *
 * @package Drupal\assignments_hootsuite\Service
 */
class HootsuiteAPIClient implements HootsuiteAPIClientInterface {
  use StringTranslationTrait;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Uneditable Config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The hootsuite access token.
   * 
   * @var string
   */
  private $accessToken;

  /**
   * The hootsuite refresh token.
   * 
   * @var string
   */
  private $refreshToken;

  /**
   * Create a new instance.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The http client.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(
    ConfigFactory $config,
    LoggerChannelFactoryInterface $loggerFactory,
    protected ClientInterface $httpClient,
    protected Messenger $messenger,
    protected StateInterface $state,
  ) {
    $this->config = $config->get('assignments_hootsuite.settings');
    $this->logger = $loggerFactory->get('assignments_hootsuite');
  }

  /**
   * {@inheritdoc}
   */
  public function createAuthUrl(): string {
    $params = [
      'response_type' => 'code',
      'client_id' => $this->config->get('client_id'),
      'redirect_uri' => 'http' . ($_SERVER['HTTP_HOST'] ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/assignments_hootsuite/callback',
      'scope' => 'offline',
    ];
    return $this->config->get('url_auth_endpoint') . '?' . http_build_query($params);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessTokenByAuthCode(string $code = NULL): bool {
    if ($code != NULL) {
      $request_options = [
        RequestOptions::HEADERS => [
          'Authorization' => 'Basic ' . base64_encode($this->config->get('client_id') . ':' . $this->config->get('client_secret')),
          'Accept' => '*/*',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        RequestOptions::FORM_PARAMS => [
          'code' => $code,
          'redirect_uri' => 'http' . ($_SERVER['HTTP_HOST'] ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/assignments_hootsuite/callback',
          'grant_type' => 'authorization_code',
          'scope' => 'offline',
        ],
      ];
      try {
        $token = $this->httpClient->request('POST',
              $this->config->get('url_token_endpoint'), $request_options);
      }
      catch (\Exception $e) {
        $this->logger->error(
          'Could not acquire token due to "%error"',
          ['%error' => $e->getMessage()]
        );
        $this->messenger->addError(
          $this->t(
            'Could not acquire token due to "%error"',
            ['%error' => $e->getMessage()]
          )
        );
        return FALSE;
      }
      $response = Json::decode($token->getBody());
      if ($token->getStatusCode() == 200 && isset($response['access_token'])) {
        $this->state->set('hootsuite_access_token', $response['access_token']);
        $this->state->set('hootsuite_refresh_token', $response['refresh_token']);

        return TRUE;
      }
    }
    elseif (!empty($this->state->get('hootsuite_refresh_token'))) {

      $request_options = [
        RequestOptions::HEADERS => [
          'Authorization' => 'Basic ' . base64_encode($this->config->get('client_id') . ':' . $this->config->get('client_secret')),
          'Accept' => '*/*',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        RequestOptions::FORM_PARAMS => [
          'refresh_token' => $this->state->get('hootsuite_refresh_token'),
          'redirect_uri' => 'http' . ($_SERVER['HTTP_HOST'] ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/assignments_hootsuite/callback',
          'grant_type' => 'refresh_token',
          'scope' => 'offline',
        ],
      ];
      // Refresh token.
      try {
        $token = $this->httpClient->request('post', $this->config->get('url_token_endpoint'), $request_options);
      }
      catch (\Exception $e) {
        $this->logger->error('Could not refresh token due to "%error"', ['%error' => $e->getMessage()]);
        $this->messenger->addError($this->t('Could not refresh token due to "%error"', ['%error' => $e->getMessage()]));
        return FALSE;
      }
      $response = Json::decode($token->getBody());
      if ($token->getStatusCode() == 200 && $response['access_token']) {
        $this->state->set('hootsuite_access_token', $response['access_token']);
        $this->state->set('hootsuite_refresh_token', $response['refresh_token']);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function connect(string $method, string $endpoint, string $query = NULL, array $body = NULL): mixed {

    $accessToken = $this->state->get('hootsuite_access_token');
    $request_options = [
      RequestOptions::HEADERS => [
        'Authorization' => 'Bearer ' . $accessToken,
        'Content-Type' => 'application/json',
        'Accept' => '*/*',
      ],
    ];
    if (!empty($body)) {
      $data = json_encode($body);
      $request_options[RequestOptions::BODY] = $data;
    }
    if (!empty($query)) {
      $request_options[RequestOptions::QUERY] = $query;
    }
    try {
      $response = $this->httpClient->{$method}(
            $endpoint,
            $request_options
        );
    }
    catch (\Exception $exception) {
      if (strpos($exception->getMessage(), "400 Bad Request") !== FALSE) {
        $this->logger->error(
          'Failed to complete taks "%method" with error "%error"',
          [
            '%method' => $method,
            '%error' => $exception->getMessage(),
          ]
        );
        return FALSE;
      }
      if (strpos($exception->getMessage(), "401 Unauthorized") !== FALSE) {
        // Refresh token and resend request.
        if ($this->getAccessTokenByAuthCode()) {
          return $this->connect($method, $endpoint, $query, $body);
        }
      }
      $this->logger->error('Failed to complete Planning Center Task "%error"', ['%error' => $exception->getMessage()]);
      return FALSE;
    }

    // Token expired.
    if ($response->getStatusCode() == 400 || $response->getStatusCode() == 401 || $response->getStatusCode() == 403) {
      // Refresh token and resend request.
      if ($this->getAccessTokenByAuthCode()) {
        return $this->connect($method, $endpoint, $query, $body);
      }
    }
    // @todo Possibly allow returning the whole body.
    return $response->getBody();
  }

  /**
   * Get the http client.
   *
   * @return \GuzzleHttp\ClientInterface
   *   The http client.
   */
  public function getHttpClient(): ClientInterface {
    return $this->httpClient;
  }

}
