<?php

namespace Drupal\assignments_hootsuite\Service;

use GuzzleHttp\ClientInterface;

/**
 * Hootsuite API client interface.
 *
 * @package Drupal\assignments_hootsuite\Service
 */
interface HootsuiteAPIClientInterface {

  /**
   * Create the authentication url based on config.
   *
   * @return string
   *   The authentication url.
   */
  public function createAuthUrl(): string;

  /**
   * Get and save access tokens.
   *
   * @param string $code
   *   The code to use to get the access token.
   *
   * @return bool
   *   Whether to access token has been acquired.
   */
  public function getAccessTokenByAuthCode(string $code = NULL): bool;

  /**
   * Connect to api to send or retrieve data.
   *
   * @param string $method
   *   The method to use (get, post, put etc.)
   * @param string $endpoint
   *   The endpoint to call.
   * @param string $query
   *   The query parameters to send (optional).
   * @param array $body
   *   The body to send (optional).
   *
   * @return mixed
   *   The response body.
   */
  public function connect(string $method, string $endpoint, string $query = NULL, array $body = NULL): mixed;

  /**
   * Get the http client.
   *
   * @return \GuzzleHttp\ClientInterface
   *   The http client.
   */
  public function getHttpClient(): ClientInterface;

}
