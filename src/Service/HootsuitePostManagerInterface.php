<?php

namespace Drupal\assignments_hootsuite\Service;

use Drupal\assignments\Entity\Assignment;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;

/**
 * Class Hootsuite Post Manager.
 *
 * @package Drupal\assignments_hootsuite\Service
 */
interface HootsuitePostManagerInterface {

  /**
   * Handle a node.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The node to handle.
   * @param bool $update
   *   Whether the node is being updated.
   */
  public function handleNode(NodeInterface $entity, $update = FALSE);

  /**
   * Send an assignment to hootsuite.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The node to post the assignment of.
   * @param \Drupal\assignments\Entity\Assignment $assignment
   *   The assignment to post.
   */
  public function sendPost(NodeInterface &$entity, Assignment &$assignment);

  /**
   * Delete an assignment from Hootsuite.
   *
   * @param \Drupal\assignments\Entity\Assignment $assignment
   *   The assignment to delete.
   * @param bool $update
   *   Whether to post info about the deletion.
   */
  public function deletePost(Assignment &$assignment, $update = FALSE);

  /**
   * Upload image to hootsuite.
   *
   * @param \Drupal\file\Entity\File $image
   *   The file to upload.
   *
   * @return string|null
   *   The image id or null.
   */
  public function uploadImage(File $image): ?string;

  /**
   * Get profile metadata.
   *
   * @param \Drupal\assignments\Entity\Assignment $assignment
   *   The assignment.
   *
   * @return array
   *   The profile metadata or null.
   */
  public function getProfileMetadata(Assignment $assignment): ?array;

}
