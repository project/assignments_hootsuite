<?php

namespace Drupal\assignments_hootsuite\Service;

use Drupal\assignments\Entity\Assignment;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use GuzzleHttp\RequestOptions;

/**
 * Class Hootsuite Post Manager.
 *
 * @package Drupal\assignments_hootsuite\Service
 */
class HootsuitePostManager implements HootsuitePostManagerInterface {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config = NULL;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger = NULL;

  /**
   * The field name for assignment.
   *
   * @var string
   */
  protected $assignmentField = 'field_hs_assignment';

  /**
   * The images to upload.
   *
   * @var array
   */
  protected $images = [];

  /**
   * Create a new instance.
   *
   * @param HootsuiteAPIClient $hootsuiteClient
   *   The api client for hootsuite.
   * @param \Drupal\Core\Utility\Token $tokenService
   *   The token service.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   */
  public function __construct(
    protected HootsuiteAPIClientInterface $hootsuiteClient,
    protected Token $tokenService,
    ConfigFactory $config,
    LoggerChannelFactoryInterface $loggerFactory,
    protected Messenger $messenger,
  ) {
    $this->config = $config->get('assignments_hootsuite.settings');
    $this->logger = $loggerFactory->get('assignments_hootsuite');
  }

  /**
   * {@inheritdoc}
   */
  public function handleNode(NodeInterface $entity, $update = FALSE) {
    if ($entity->hasField($this->assignmentField)) {
      $assignments = $entity->get($this->assignmentField);
      if (count($assignments) > 0) {
        foreach ($assignments as $item) {
          if ($item->entity != NULL && $item->entity->hasField('field_hs_profile_id')) {
            if ($this->validate($entity, $item->entity)) {
              $this->sendPost($entity, $item->entity);
            }
          }
        }
      }
      if ($update) {
        // Detect removed assignments by comparing
        // assignments in original entity to assignments in updated entity.
        $old_assignments = $entity->original->get($this->assignmentField)->filter(
          fn($assignment) =>
            (!empty($assignment->entity)) &&
            !in_array($assignment->entity->id(), array_column($assignments->getValue(), 'target_id'))
          );
        // Delete assignments not in updated entity.
        foreach ($old_assignments as $old_assignment) {
          $this->deletePost($old_assignment->entity, $update);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendPost(NodeInterface &$entity, Assignment &$assignment) {
    // Delete post from hootsuite if already exists.
    if (!$assignment->field_hs_post_id->isEmpty()) {
      $this->deletePost($assignment, TRUE);
      $assignment->field_hs_post_id->value = NULL;
    }

    $entityData = [
      'node' => $entity,
    ];

    $requestBody = [
      'text' => html_entity_decode(strip_tags($this->tokenService->replace($assignment->field_hs_post->value, $entityData, ['clear' => TRUE]))),
      'socialProfileIds' => [$assignment->field_hs_profile_id->value],
      'scheduledSendTime' => $assignment->field_hs_date->value . 'Z',
    ];

    if (!$assignment->field_hs_image->isEmpty()) {
      $imageId = $this->tokenService->replace($assignment->field_hs_image->value, $entityData, ['clear' => TRUE]);
      $image = File::load($imageId);

      if ($image != NULL) {
        // Check if image is square for instagram.
        $profile_meta_data = $this->getProfileMetadata($assignment);
        if (stripos($profile_meta_data['type'], 'instagram') !== FALSE && !$this->checkImageSquare($image)) {
          $this->messenger->addError(
              $this->t('Post for @profile could not be posted as the image needs to be square for instagram.',
                ['@profile' => $assignment->field_hs_profile_name->value])
            );
          return;
        }

        // Upload image to aws.
        if (($id = $this->uploadImage($image)) !== FALSE) {
          $requestBody['media'] = [['id' => $id]];
        }
        else {
          $this->messenger->addError(
             $this->t('Post for @profile has not been posted/changed on Hootsuite due to error on image processing.',
              ['@profile' => $assignment->field_hs_profile_name->value])
            );
          return;
        }
      }
    }

    // Add special data for pinterest.
    if (!empty($extendedInfo = $this->augmentForPinterest($entity, $assignment))) {
      $requestBody['extendedInfo'] = $extendedInfo;
    }

    // Post to hootsuite.
    $response = $this->hootsuiteClient->connect('post', $this->config->get('url_post_message_endpoint'), NULL, $requestBody);
    if (empty($response)) {
      $this->messenger->addError(
        $this->t('Post for @profile has not been posted/changed on Hootsuite due to error on post creation. Check logs for more info.',
          ['@profile' => $assignment->field_hs_profile_name->value])
      );
      return;
    }

    // Process response.
    $data = Json::decode($response, TRUE);
    if (!empty($data['data'][0]) && $data['data'][0]['state'] == 'SCHEDULED') {
      $hootsuite_post_id = $data['data'][0]['id'];
      $assignment->field_hs_post_id = $hootsuite_post_id;
      /** @var \Drupal\Core\Entity\Entity $entity */
      $assignment->save();
      $this->logger->notice(
       $this->t('Created post for @profile with id @id.',
        [
          '@profile' => $assignment->field_hs_profile_name->value,
          '@id' => $assignment->field_hs_post_id->value,
        ]
       )
      );
      $this->messenger->addMessage(
        $this->t('The post for @profile has been successfully scheduled.',
          [
            '@profile' => $assignment->field_hs_profile_name->value,
          ]
        )
      );
    }
    else {
      $this->logger->notice(
        $this->t('Fail creating post for @profile with id @id.',
         [
           '@profile' => $assignment->field_hs_profile_name->value,
           '@id' => $assignment->field_hs_post_id->value,
         ]
        )
       );
      $this->messenger->addWarning(
        $this->t('Failed posting for @profile.',
          [
            '@profile' => $assignment->field_hs_profile_name->value,
          ]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePost(Assignment &$assignment, $update = FALSE) {
    if (!$assignment->hasField('field_hs_post_id') || $assignment->field_hs_post_id->isEmpty()) {
      return;
    }
    if ($assignment->field_hs_date->date->getTimestamp() < time()) {
      return;
    }
    $url = $this->config->get('url_post_message_endpoint') . '/' . $assignment->field_hs_post_id->value;
    $this->hootsuiteClient->connect('delete', $url);
    if (!$update) {
      $this->logger->notice(
       $this->t('Deleted post for @profile with id @id.',
        [
          '@profile' => $assignment->field_hs_profile_name->value,
          '@id' => $assignment->field_hs_post_id->value,
        ]
       )
      );
      $this->messenger->addMessage(
        $this->t('Deleted post for @profile.',
          [
            '@profile' => $assignment->field_hs_profile_name->value,
          ]
        )
      );
    }
  }

  /**
   * Get extended info for Pinterested.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The entity.
   * @param \Drupal\assignments\Entity\Assignment $assignment
   *   The assignment.
   */
  protected function augmentForPinterest(NodeInterface $entity, Assignment $assignment) {
    // Add special data for pinterest.
    if ($assignment->hasField('field_hs_pinterest_board')) {
      if (!$assignment->field_hs_pinterest_board->isEmpty()) {
        $pinterestUrl = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
        if (!$assignment->field_hs_pinterest_url->isEmpty()) {
          $pinterestUrl = $assignment->field_hs_pinterest_url->first()->getUrl()->toString();
        }
        $extendedInfo = [
            [
              'socialProfileType' => 'PINTEREST',
              'socialProfileId' => $assignment->field_hs_profile_id->value,
              'data' => [
                'boardId' => $assignment->field_hs_pinterest_board->value,
                'destinationUrl' => $pinterestUrl,
              ],
            ],
        ];
        return $extendedInfo;
      }
      else {
        $this->messenger->addError(
         $this->t('Post for @profile has not been posted/changed on Hootsuite due to missing board id.',
         ['@profile' => $assignment->field_hs_profile_name->value])
        );
        return NULL;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function uploadImage(File $image): ?string {
    if (!empty($this->images[$image->id()])) {
      return $this->images[$image->id()];
    }
    else {
      $id = $this->registerImage($image);
      if ($id) {
        $this->images[$image->id()] = $id;
        return $id;
      }
    }
    return FALSE;
  }

  /**
   * Register image with hootsuite.
   *
   * @param \Drupal\file\Entity\File $image
   *   The file to register.
   */
  protected function registerImage(File $image) {
    $body = [
      'mimeType' => $image->getMimeType(),
      'sizeBytes' => filesize($image->getFileUri()),
    ];
    $response = $this->hootsuiteClient->connect('post', $this->config->get('url_post_media_endpoint'), NULL, $body);
    if (empty($response)) {
      return FALSE;
    }
    $data = Json::decode($response);
    if (!empty($data['data']) && $data = $data['data']) {
      $id = $data['id'];
      if ($this->uploadToAws($image, $data['uploadUrl'])) {
        $i = 0;
        do {
          sleep(1);
          $i++;
          if ($i > 20) {
            $this->messenger->addMessage('Image could not be uploaded, waited for 20 seconds', 'warning');
            $this->logger->warning('Timeout on ready state for image with id @id.', ['@id' => $id]);
            return FALSE;
          }
          $response = $this->hootsuiteClient->connect('get', $this->config->get('url_post_media_endpoint') . '/' . $id);
          if (empty($response)) {
            return FALSE;
          }
          $data = Json::decode($response->getContents());
          if (!empty($data['data']['state'])) {
            $state = $data['data']['state'];
          }
          else {
            $state = '';
          }
        } while ($state != 'READY');
        $this->logger->notice('Ready state for image id @id.', ['@id' => $id]);
        return $id;
      }
    }
    return FALSE;
  }

  /**
   * Upload an image to aws.
   *
   * @param \Drupal\file\Entity\File $image
   *   The file to upload.
   * @param string $url
   *   The endpoint to send it to.
   */
  protected function uploadToAws(File $image, string $url) {
    $requestOptions = [
      RequestOptions::HEADERS => [
        'Content-Type' => $image->getMimeType(),
        'Content-Length' => filesize($image->getFileUri()),
      ],
      RequestOptions::BODY => fopen($image->getFileUri(), 'r'),
    ];
    try {
      $this->hootsuiteClient->getHttpClient()->request('put', $url, $requestOptions);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->messenger->addMessage($e->getMessage());
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Check, if entity is or will be published by post time.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The entity.
   * @param \Drupal\assignments\Entity\Assignment $assignment
   *   The assignment.
   */
  protected function validate(NodeInterface &$entity, Assignment &$assignment) {
    if (!$entity->isPublished()) {
      if ($entity->hasField('publish_on') && !$entity->publish_on->isEmpty()) {
        if ($assignment->field_hs_date->date->getTimestamp() < $entity->publish_on->value) {
          $this->messenger->addWarning(
            $this->t(
              'Cannot schedule post for @profile before the entry is being published.',
              ['@name' => $assignment->field_hs_profile_name->value]
            )
          );
          return FALSE;
        }
      }
      else {
        $this->messenger->addWarning(
          $this->t(
            'Cannot schedule post for @profile for unpublished entry.',
            ['@name' => $assignment->field_hs_profile_name->value]
          )
        );
        return FALSE;
      }
    }
    if ($assignment->field_hs_date->date->getTimestamp() < time()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Check if image is square.
   *
   * @param \Drupal\file\Entity\File $image
   *   The image to check.
   *
   * @return bool
   *   Whether the image is square.
   */
  protected function checkImageSquare(File $image) {
    $imageUri = $image->getFileUri();
    $imageInfo = getimagesize($imageUri);
    if ($imageInfo[0] === $imageInfo[1]) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getProfileMetadata(Assignment $assignment): array {
    // Get meta data of profile.
    $endpoint = $this->config->get('url_social_profiles_endpoint') . '/' . $assignment->field_hs_profile_id->value;
    $response = $this->hootsuiteClient->connect('get', $endpoint);
    if (empty($response)) {
      return [];
    }
    $data = Json::decode($response);
    if (!empty($data['data'])) {
      return $data['data'];
    }
    return [];
  }

}
